package com.streamAPI;

import java.util.List;

public class Cache {

    Student student1 = new Student(1, "alona", "lastname", 1989, "Kharkiv", "+38097", "biology", "first", "g1");
    Student student2 = new Student(2, "mark", "lastname", 1999, "Kharkiv", "+38097", "biology", "first", "g1");
    Student student3 = new Student(3, "vlad", "lastname", 1992, "Dnepr", "+38097", "biology", "second", "g2");
    Student student4 = new Student(4, "david", "lastname", 1994, "Kharkiv", "+38097", "law", "first", "g1");
    Student student5 = new Student(5, "kristina", "lastname", 1992, "Kharkiv", "+38097", "biology", "second", "g2");
    Student student6 = new Student(6, "vova", "lastname", 1993, "Dnepr", "+38097", "medicine", "third", "g3");
    Student student7 = new Student(7, "nikita", "lastname", 1993, "Kharkiv", "+38097", "law", "fourth", "g4");
    Student student8 = new Student(8, "ira", "lastname", 1991, "Kiyv", "+38097", "medicine", "third", "g3");
    Student student9 = new Student(9, "marina", "lastname", 1988, "Kiyv", "+38097", "medicine", "third", "g3");

    List<Student> listOfStudents = List.of(student1, student2, student3, student4, student5, student6, student7, student8, student9);

    public List<Student> getListOfStudents () {
        return this.listOfStudents;
    }

}
